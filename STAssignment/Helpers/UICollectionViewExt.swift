//
//  UICollectionViewExt.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

extension UICollectionView {
    
    // MARK: - Register methods -
    func registerHeader(_ headerType: UICollectionReusableView.Type) {
        register(headerType.self,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                 withReuseIdentifier: headerType.reuseIdentifier)
    }
    
    func registerCell(_ cellType: UICollectionViewCell.Type) {
        register(cellType.self, forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    func registerFooter(_ footerType: UICollectionReusableView.Type) {
        register(footerType.self,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                 withReuseIdentifier: footerType.reuseIdentifier)
    }
    
    // MARK: - Dequeue methods -
    
    func dequeueReusableHeaderView<HeaderView: UICollectionReusableView>(forIndexPath indexPath: IndexPath) -> HeaderView {
        guard let headerView = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                withReuseIdentifier: HeaderView.reuseIdentifier,
                                                                for: indexPath) as? HeaderView else {
            fatalError("Fatal error for headerView at \(indexPath)")
        }
        return headerView
    }
    
    func dequeueReusableCell<Cell: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {
            fatalError("Fatal error for cell at \(indexPath)")
        }
        return cell
    }
    
    func dequeueReusableFooterView<FooterView: UICollectionReusableView>(forIndexPath indexPath: IndexPath) -> FooterView {
        guard let footerView = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                                withReuseIdentifier: FooterView.reuseIdentifier,
                                                                for: indexPath) as? FooterView else {
            fatalError("Fatal error for footerView at \(indexPath)")
        }
        return footerView
    }
    
}
