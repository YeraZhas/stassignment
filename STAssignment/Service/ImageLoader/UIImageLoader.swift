//
//  UIImageLoader.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import UIKit

final class UIImageLoader {
    
    static let shared = UIImageLoader()
    private let imageLoader = ImageLoader()
    private var uuidMap: [UIImageView: UUID] = [:]
    
    private  init() {}
    
    func load(_ url: URL, for imageView: UIImageView) {
        let token = imageLoader.loadImage(url) { result in
            defer { self.uuidMap.removeValue(forKey: imageView) }
            guard let image = try? result.get() else { return }
            DispatchQueue.main.async {
                imageView.image = image
            }
        }
        if let token = token {
            uuidMap[imageView] = token
        }
    }
    
    func cancel(for imageView: UIImageView) {
        if let uuid = uuidMap[imageView] {
            imageLoader.cancelLoad(uuid)
            uuidMap.removeValue(forKey: imageView)
        }
    }
    
}
