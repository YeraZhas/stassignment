//
//  MockBookRepository.swift
//  STAssignmentTests
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import Foundation
@testable import STAssignment

final class MockBookRepository: BookRepositoryProtocol {
    
    var result: Result<PaginationModel<BookModel>, Error>
    var didFinishRequestAction: (([BookModel]) -> Void)?
    
    init(result: Result<PaginationModel<BookModel>, Error>) {
        self.result = result
    }
    
    func getBooks(nextPageToken: String, completion: @escaping (Result<PaginationModel<BookModel>, Error>) -> Void) {
        completion(self.result)
        didFinishRequestAction?((try? self.result.get().items) ?? [])
    }
    
}
