//
//  BookRepository.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

final class BookRepository: BookRepositoryProtocol {
    
    private let networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getBooks(nextPageToken: String, completion: @escaping (Result<PaginationModel<BookModel>, Error>) -> Void) {
        let router = BookRouter.getBooks(nextPageToken: nextPageToken)
        networkManager.makeRequest(from: router, completion: completion)
    }
    
}
