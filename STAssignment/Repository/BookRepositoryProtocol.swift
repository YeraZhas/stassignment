//
//  BookRepositoryProtocol.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import Foundation

protocol BookRepositoryProtocol {
    
    func getBooks(nextPageToken: String, completion: @escaping (Result<PaginationModel<BookModel>, Error>) -> Void)
    
}
