//
//  BookVMTests.swift
//  STAssignmentTests
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import XCTest
@testable import STAssignment

class BookVMTests: XCTestCase {
    
    var viewModel: BookVM!

    func testNonEmptyAuthorsAndNarratorsStringFormat() {
        let bookModel = mockNonEmptyBook()
        viewModel = BookVM(bookModel: bookModel)
        
        XCTAssert(viewModel.authors == "by DUMMY, DUMMY, DUMMY", "if any authors, string should be prepended with by nd joined with comma")
        XCTAssert(viewModel.narrators == "with DUMMY, DUMMY", "if any narrators, string should be prepended with by and joined with comma")
    }
    
    func testEmptyAuthorsAndNarratorsStringFormat() {
        let bookModel = mockEmptyBook()
        viewModel = BookVM(bookModel: bookModel)
        
        XCTAssert(viewModel.authors == nil, "if no authors, string should be nil")
        XCTAssert(viewModel.narrators == nil, "if no narrators, string should be nil")
    }
    
    // MARK - Helper methods -
    
    private func mockEmptyBook() -> BookModel {
        .init(title: "DUMMY", authors: [], narrators: [], cover: .init(url: ""))
    }

    private func mockNonEmptyBook() -> BookModel {
        let authors = [makeAuthor(), makeAuthor(), makeAuthor()]
        let narrators = [makeNarrator(), makeNarrator()]
        return .init(title: "DUMMY", authors: authors, narrators: narrators, cover: .init(url: ""))
    }
    
    private func makeAuthor() -> BookModel.AuthorModel {
        .init(name: "DUMMY")
    }
    
    private func makeNarrator() -> BookModel.NarratorModel {
        .init(name: "DUMMY")
    }

}
