//
//  NetworkManager.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

final class NetworkManager {
    
    private var task: URLSessionTask?
    private let parser: Parser
    
    init(parser: Parser) {
        self.parser = parser
    }
    
    func makeRequest<T: Decodable>(from router: NetworkRouter, completion: @escaping (Result<T, Error>) -> Void) {
        guard let request = router.buildRequest() else {
            completion(.failure(NetworkError.invalidRequest))
            return
        }
        task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            completion(self.parser.parse(data: data, response: response, error: error))
        }
        self.task?.resume()
    }
    
}
