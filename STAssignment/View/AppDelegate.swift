//
//  AppDelegate.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        // better use dependency injection container
        let networkManager = NetworkManager(parser: DefaultParser())
        let viewModel = BookShelfVM(bookRepository: BookRepository(networkManager: networkManager))
        let rootVC = BookShelfViewController(viewModel: viewModel)
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
        
        return true
    }

}

