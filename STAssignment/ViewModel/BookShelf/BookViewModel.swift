//
//  BookViewModel.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

final class BookVM {
    
    let title: String
    let authors: String
    let narrators: String
    
    init(title: String, authors: String, narrators: String) {
        self.title = title
        self.authors = "by \(authors)"
        self.narrators = "with \(narrators)"
    }
    
}
