//
//  BookRepositoryTests.swift
//  STAssignmentTests
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import XCTest
@testable import STAssignment

class BookRepositoryTests: XCTestCase {

    var bookRepository: BookRepositoryProtocol!
    
    override func setUpWithError() throws {
        bookRepository = BookRepository(networkManager: NetworkManager(parser: DefaultParser()))
    }
    
    // real server request test
    func testRepositoryFetchesBooks() {
        let expectation = self.expectation(description: "books fetch")
        bookRepository.getBooks(nextPageToken: "10") { result in
            guard let paginationModel = try? result.get() else {
                return
            }
            XCTAssert(paginationModel.items.count == 10, "Items count should be 10")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)
    }
    
    func testRespositoryFetchesMockedBooks() {
        let paginationModel: PaginationModel<BookModel> = mockPaginationModel()
        let result: Result<PaginationModel<BookModel>, Error> = .success(paginationModel)
        let mockBookRepository = MockBookRepository(result: result)
        
        let expectation = self.expectation(description: "mock books fetch")
        mockBookRepository.getBooks(nextPageToken: "0") { result in
            guard let paginationModel = try? result.get() else {
                XCTFail()
                return
            }
            XCTAssert(paginationModel.items.count == 10)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
    }
    
    private func mockPaginationModel() -> PaginationModel<BookModel> {
        func mockBooks() -> [BookModel] {
            [
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY")),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: "DUMMY"))
            ]
        }
        
        let books: [BookModel] = mockBooks()
        return PaginationModel(query: "query", nextPageToken: "token", totalCount: 10, items: books)
    }
    
}
