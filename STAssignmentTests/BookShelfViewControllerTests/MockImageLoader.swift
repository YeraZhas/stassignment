//
//  MockImageLoader.swift
//  STAssignmentTests
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import UIKit
@testable import STAssignment

final class MockImageLoader: ImageLoaderProtocol {
    
    func loadImage(_ url: URL, _ completion: @escaping (Result<UIImage, Error>) -> Void) -> UUID? {
        return nil
    }
    
    func cancelLoad(_ uuid: UUID) {}
    
}
