//
//  BookShelfVM.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

protocol BookShelfVMDelegate: AnyObject {
    
    func bookShelfVMDidReceiveBooksAtIndexPaths(_ indexPaths: [IndexPath])
    func bookShelfVMDidReceiveError(_ error: Error)
    func bookShelfVMHasFinishedPagination()
}

final class BookShelfVM {
    
    weak var delegate: BookShelfVMDelegate?
    
    var hasFinishedPagination = false
    var hasLoadedOnce = false
    var hasLoaderAppeared = false
    
    private(set) var bookVMs: [BookVM] = []
    private let bookRepository: BookRepositoryProtocol
    private var nextPageToken: String? = "1360"
    private var indexPathsCount: Int = 0
    
    init(bookRepository: BookRepositoryProtocol) {
        self.bookRepository = bookRepository
    }
    
    func getBooks() {
        guard let nextPageToken = nextPageToken else {
            return
        }
        bookRepository.getBooks(nextPageToken: nextPageToken) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let paginationModel):
                    let newBookVMs = paginationModel.items.map { BookVM(bookModel: $0) }
                    let newBooksCount = newBookVMs.count
                    self.bookVMs.append(contentsOf: newBookVMs)
                    let oldIndexPathsCount = self.indexPathsCount
                    let newIndexPathsCount = max(0, newBooksCount + oldIndexPathsCount - 1)
                    self.indexPathsCount = newIndexPathsCount + 1
                    if newIndexPathsCount > oldIndexPathsCount {
                        self.delegate?.bookShelfVMDidReceiveBooksAtIndexPaths(
                            (oldIndexPathsCount...newIndexPathsCount).map {
                                IndexPath(item: $0, section: 0)
                            }
                        )
                    } else {
                        self.delegate?.bookShelfVMDidReceiveBooksAtIndexPaths([])
                    }
                    if paginationModel.nextPageToken == nil {
                        self.finishPagination()
                    } else {
                        self.nextPageToken = paginationModel.nextPageToken
                    }
                case .failure(let error):
                    self.finishPagination()
                    self.delegate?.bookShelfVMDidReceiveError(error)
                }
            }
        }
    }
    
    private func finishPagination() {
        self.hasFinishedPagination = true
        self.nextPageToken = nil
        self.delegate?.bookShelfVMHasFinishedPagination()
    }
    
}
