//
//  BookShelfViewControllerTests.swift
//  STAssignmentTests
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import XCTest
@testable import STAssignment

class BookShelfViewControllerTests: XCTestCase {
    
    var vc: BookShelfViewController!
    var bookRepository: MockBookRepository!
    
    override func setUpWithError() throws {
        let paginationModel: PaginationModel<BookModel> = mockPaginationModel()
        let result: Result<PaginationModel<BookModel>, Error> = .success(paginationModel)
        bookRepository = MockBookRepository(result: result)
        let vm = BookShelfVM(bookRepository: bookRepository)
        vc = BookShelfViewController(viewModel: vm)
    }
    
    override func tearDownWithError() throws {
        vc = nil
        bookRepository = nil
    }

    func testBookRepositoryBooksFetch() {
        let expectation = self.expectation(description: "Downloading books")
        bookRepository.didFinishRequestAction = { books in
            XCTAssert(books.count == 10, "The number of books should be 10")
            expectation.fulfill()
        }
        vc.loadViewIfNeeded()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testBookShelfVCDataSource() {
        XCTAssert(vc.collectionView.dataSource is BookShelfViewController)
    }
    
    func testNumberOfItemsAfterFetchingBooks() {
        let expectation = self.expectation(description: "Downloading books")
        vc.loadViewIfNeeded()
        print("number of items in section 0: \(self.vc.collectionView.numberOfItems(inSection: 0))")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            XCTAssert(self.vc.collectionView.numberOfItems(inSection: 0) == 10)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testNumberOfSectionsWhilePaginationHasFinished() {
        
    }
    
    private func mockPaginationModel() -> PaginationModel<BookModel> {
        func mockBooks() -> [BookModel] {
            let coverURLString = "https://www.storytel.com/images/9781781103265/640x640/cover.jpg"
            return [
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString)),
                .init(title: "DUMMY", authors: [], narrators: [], cover: BookModel.CoverModel(url: coverURLString))
            ]
        }
        
        let books: [BookModel] = mockBooks()
        return PaginationModel(query: "query", nextPageToken: nil, totalCount: 10, items: books)
    }

}
