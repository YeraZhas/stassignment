//
//  BookModel.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

struct BookModel: Decodable {
    
    struct PersonModel: Decodable {
        
        let name: String
        
    }
    
    struct CoverModel: Decodable {
        
        let url: String
        
    }
    
    typealias AuthorModel = PersonModel
    typealias NarratorModel = PersonModel
    
    let title: String
    let authors: [AuthorModel]
    let narrators: [NarratorModel]
    let cover: CoverModel
    
}
