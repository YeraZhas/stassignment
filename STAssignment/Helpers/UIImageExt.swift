//
//  UIImageExt.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import UIKit

extension UIImageView {
    
    func loadImage(from url: URL) {
        UIImageLoader.shared.load(url, for: self)
    }
    
    func cancelImageLoading() {
        UIImageLoader.shared.cancel(for: self)
    }
    
}
