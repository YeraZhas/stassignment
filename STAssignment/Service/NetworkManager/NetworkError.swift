//
//  NetworkError.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

enum NetworkError: String, Error {
    
    case invalidHTTPResponse
    case noResponseData
    case invalidRequest
    
}

extension NetworkError: LocalizedError {
    
    //should localize strings
    var errorDescription: String? {
        "Error"
    }
    
    var failureReason: String? {
        switch self {
        case .noResponseData:
            return "No data was sent with network response"
        case .invalidHTTPResponse:
            return "Received invalid network response"
        case .invalidRequest:
            return "Network request was failed because of undefined error"
        }
    }
    
}
