//
//  NetworkRouter.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

protocol NetworkRouter {
    
    var httpMethod: String { get }
    var path: String { get }
    var parameters: [String: Any] { get }
    
    func buildRequest() -> URLRequest?
    
}

struct BookRouter: NetworkRouter {
    
    var httpMethod: String
    let path: String
    let parameters: [String : Any]
    
    private init(httpMethod: String, path: String, parameters: [String: Any]) {
        self.httpMethod = httpMethod
        self.path = path
        self.parameters = parameters
    }
    
    // MARK: - Endpoints -
    
    static func getBooks(nextPageToken: String) -> BookRouter {
        return .init(httpMethod: "GET",
                     path: "https://api.storytel.net/search",
                     parameters: ["page": nextPageToken,
                                  "query": "harry"])
    }
    
    // MARK: - Building request -
    
    func buildRequest() -> URLRequest? {
        guard let url = URL(string: path) else { return nil }
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod
        
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
                urlComponents.queryItems?.append(queryItem)
            }
            request.url = urlComponents.url
        }
        return request
    }
    
}
