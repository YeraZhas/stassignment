//
//  Reusable.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

// MARK: - Reusable protocol definition -

protocol Reusable {}

extension Reusable where Self: UICollectionViewCell {
    
    static var reuseIdentifier: String {
        String(describing: self)
    }
    
}

extension Reusable where Self: UICollectionReusableView {
    
    static var reuseIdentifier: String {
        String(describing: self)
    }
    
}

// MARK: - UIView classes conformance to Reusable protocol -

extension UICollectionReusableView: Reusable {}
