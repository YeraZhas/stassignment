//
//  BookCell.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

final class BookCell: UICollectionViewCell {
    
    private var viewModel: BookVM?
    private lazy var infoStackView: UIStackView = {
        let isv = UIStackView()
        isv.axis = .vertical
        isv.spacing = 5
        return isv
    }()
    
    private lazy var coverImageView: UIImageView = {
        let civ = UIImageView()
        civ.contentMode = .scaleAspectFit
        civ.clipsToBounds = true
        civ.backgroundColor = .lightGray
        return civ
    }()
    
    private lazy var titleLabel: UILabel = {
        let tl = UILabel()
        tl.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        tl.textAlignment = .left
        return tl
    }()
    
    private lazy var authorsLabel: UILabel = {
        let al = UILabel()
        al.font = UIFont.systemFont(ofSize: 12)
        al.textColor = .lightGray
        al.textAlignment = .left
        return al
    }()
    
    private lazy var narratorsLabel: UILabel = {
        let nl = UILabel()
        nl.font = UIFont.systemFont(ofSize: 12)
        nl.textColor = .lightGray
        nl.textAlignment = .left
        return nl
    }()
    
    private lazy var vSpacer: UIView = {
        let vs = UIView()
        vs.setContentHuggingPriority(.defaultLow, for: .vertical)
        return vs
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let bsv = UIView()
        bsv.backgroundColor = .lightGray
        return bsv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        coverImageView.image = nil
        coverImageView.cancelImageLoading()
    }
    
    func configure(with viewModel: BookVM) {
        self.viewModel = viewModel
        titleLabel.text = viewModel.title
        authorsLabel.text = viewModel.authors
        narratorsLabel.text = viewModel.narrators
        guard let coverURL = viewModel.coverURL else { return }
        coverImageView.loadImage(from: coverURL)
    }
    
    private func setupViews() {
        backgroundColor = .white
        
        addSubviews([coverImageView, infoStackView, bottomSeparatorView])
        
        coverImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
        coverImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        coverImageView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        coverImageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        infoStackView.topAnchor.constraint(equalTo: coverImageView.topAnchor).isActive = true
        infoStackView.leadingAnchor.constraint(equalTo: coverImageView.trailingAnchor, constant: 10).isActive = true
        infoStackView.bottomAnchor.constraint(equalTo: coverImageView.bottomAnchor).isActive = true
        infoStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
        
        bottomSeparatorView.leadingAnchor.constraint(equalTo: coverImageView.leadingAnchor).isActive = true
        bottomSeparatorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.6).isActive = true
        bottomSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        infoStackView.addArrangedSubviews([titleLabel, authorsLabel, narratorsLabel, vSpacer])
    }
    
}
