//
//  BookLoadingCell.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

final class BookLoadingCell: UICollectionViewCell {
    
    lazy var loaderView: UIActivityIndicatorView = {
        let lv = UIActivityIndicatorView()
        lv.color = .lightGray
        lv.startAnimating()
        return lv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(loaderView)
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        
        loaderView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loaderView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loaderView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        loaderView.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
}
