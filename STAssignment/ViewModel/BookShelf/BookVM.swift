//
//  BookViewModel.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

final class BookVM {
    
    let title: String
    let authors: String?
    let narrators: String?
    let coverURL: URL?
    
    init(bookModel: BookModel) {
        self.title = bookModel.title
        let authorsJoined = bookModel.authors.map { $0.name }.joined(separator: ", ")
        self.authors = authorsJoined.isEmpty ? nil : "by \(authorsJoined)"
        let narratorsJoined = bookModel.narrators.map { $0.name }.joined(separator: ", ")
        self.narrators = narratorsJoined.isEmpty ? nil : "with \(narratorsJoined)"
        self.coverURL = URL(string: bookModel.cover.url)
    }
    
}
