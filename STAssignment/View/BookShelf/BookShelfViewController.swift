//
//  ViewController.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import UIKit

final class BookShelfViewController: UIViewController {
    
    private let viewModel: BookShelfVM
    private var flowLayout = UICollectionViewFlowLayout()
    
    private lazy var loaderView: UIActivityIndicatorView = {
        let lv = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        lv.color = .lightGray
        lv.hidesWhenStopped = true
        return lv
    }()
    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        cv.dataSource = self
        cv.delegate = self
        
        return cv
    }()
    
    init(viewModel: BookShelfVM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        getBooks()
    }
    
    private func setupViews() {
        setupView()
        setupCollectionView()
        setupLoaderView()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        view.addSubviews([collectionView, loaderView])
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = .white
        collectionView.registerHeader(BookShelfHeaderView.self)
        collectionView.registerCell(BookCell.self)
        collectionView.registerCell(BookLoadingCell.self)
        
        collectionView.topAnchor.constraint(equalTo: view.topSafeAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomSafeAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        flowLayout.itemSize = CGSize(width: view.frame.width, height: 100)
    }
    
    private func setupLoaderView() {
        loaderView.startAnimating()
        loaderView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loaderView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    private func getBooks() {
        viewModel.getBooks()
    }
    
    private func showLoadingCell(_ shouldShow: Bool) {
        guard viewModel.hasLoadedOnce else { return }
        if shouldShow {
            guard !viewModel.hasLoaderAppeared else { return }
            collectionView.insertSections(IndexSet([1]))
            viewModel.hasLoaderAppeared = true
        } else {
            guard viewModel.hasFinishedPagination && viewModel.hasLoaderAppeared else { return }
            collectionView.deleteSections(IndexSet([1]))
        }
    }
    
    private func collectionViewUpdate(_ update: () -> Void) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.collectionView.performBatchUpdates {
            update()
        }
        CATransaction.commit()
    }

}

// MARK: - UICollectionViewDataSource methods -

extension BookShelfViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if viewModel.hasFinishedPagination {
            return 1
        } else {
            if viewModel.hasLoadedOnce {
                return 2
            } else {
                return 1
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.bookVMs.count
        } else {
            return 1
        }
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout methods -

extension BookShelfViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableHeaderView(forIndexPath: indexPath) as BookShelfHeaderView
            headerView.configure(with: "Query: Harry")
            return headerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: view.frame.width, height: 220)
        } else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as BookCell
            let bookVM = viewModel.bookVMs[indexPath.item]
            cell.configure(with: bookVM)
            if shouldLoadMoreBooks(indexPath) {
                getBooks()
                collectionViewUpdate {
                    showLoadingCell(true)
                }
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as BookLoadingCell
            cell.loaderView.startAnimating()
            return cell
        }
    }
    
    private func shouldLoadMoreBooks(_ indexPath: IndexPath) -> Bool {
        indexPath.item == viewModel.bookVMs.count - 1 && !viewModel.hasFinishedPagination
    }
    
}

// MARK: - BookShelfVMDelegate methods -

extension BookShelfViewController: BookShelfVMDelegate {
    
    func bookShelfVMDidReceiveBooksAtIndexPaths(_ indexPaths: [IndexPath]) {
        loaderView.stopAnimating()
        collectionViewUpdate {
            self.collectionView.insertItems(at: indexPaths)
        }
        if !viewModel.hasLoadedOnce {
            viewModel.hasLoadedOnce = true
        }
    }
    
    func bookShelfVMDidReceiveError(_ error: Error) {
        loaderView.stopAnimating()
        if let localizedError = error as? LocalizedError {
            presentAlert(error: localizedError)
        } else {
            presentAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    func bookShelfVMHasFinishedPagination() {
        collectionViewUpdate {
            self.showLoadingCell(false)
        }
    }
    
}
