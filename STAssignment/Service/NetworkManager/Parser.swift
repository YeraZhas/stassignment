//
//  Parser.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

protocol Parser {
    
    func parse<T: Decodable>(data: Data?, response: URLResponse?, error: Error?) -> Result<T, Error>
    
}

final class DefaultParser: Parser {
    
    func parse<T>(data: Data?, response: URLResponse?, error: Error?) -> Result<T, Error> where T : Decodable {
        if let error = error {
            return .failure(error)
        }
        guard let response = response as? HTTPURLResponse else {
            return .failure(NetworkError.invalidHTTPResponse)
        }
        switch response.statusCode {
        case 200...299:
            guard let data = data else { return .failure(NetworkError.noResponseData) }
            let decoder = JSONDecoder()
            do {
                let result = try decoder.decode(T.self, from: data)
                return .success(result)
            } catch {
                print("Decoding error: \(error)")
                return .failure(NetworkError.invalidHTTPResponse)
            }
        // Should handle other status codes like 400, 500 etc
        default:
            return .failure(NetworkError.invalidRequest)
        }
    }
    
}

