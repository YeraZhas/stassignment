//
//  UIViewControllerExt.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/21/21.
//

import UIKit

extension UIViewController {
    
    func presentAlert(error: LocalizedError) {
        presentAlert(title: error.localizedDescription, message: error.failureReason)
    }
    
    func presentAlert(title: String, message: String?) {
        let alertC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak alertC] _ in
            alertC?.dismiss(animated: true)
        }
        alertC.addAction(okAction)
        present(alertC, animated: true)
    }
    
}

