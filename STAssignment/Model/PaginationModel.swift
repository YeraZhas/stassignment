//
//  PaginationModel.swift
//  STAssignment
//
//  Created by Yerassyl Zhassuzakhov on 3/20/21.
//

import Foundation

struct PaginationModel<T: Decodable>: Decodable {
    
    let query: String
    let nextPageToken: String?
    let totalCount: Int
    let items: [T]
    
}
